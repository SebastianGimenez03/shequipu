import { Component, OnInit } from '@angular/core';
import { TransitiveCompileNgModuleMetadata } from '@angular/compiler';
import { travel } from 'src/models/travel.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor() { }
  travels : travel[] = [
    {tittle: "viaje tranqui",
     date: "02/10/2020",
     destination: "Noruega",
     description: "Viaje tranquilo para compartir con gente que se quiera sumar",
     icon: "car"
    },
    
    {tittle: "viaje de joda",
     date: "08/08/2021",
     destination: "Holanda",
     description: "Viaje para ir a disfrutar de joda",
     icon: "airplane-outline"
    },
    
    {tittle: "viaje en familia",
     date: "02/01/2025",
     destination: "",
     description: "Viajecito en familia para compartir",
     icon: "bus-outline"
    },
  ]

  ngOnInit() {}

}
